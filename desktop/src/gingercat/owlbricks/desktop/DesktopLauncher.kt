package gingercat.owlbricks.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import gingercat.owlbricks.MainGame

object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration()
        config.title = "Owl Bricks"
        config.width = 1080 / 2
        config.height = 1920 / 2
        LwjglApplication(MainGame(), config)
    }
}
