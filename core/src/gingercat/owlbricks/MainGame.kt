package gingercat.owlbricks

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.utils.viewport.ExtendViewport
import com.badlogic.gdx.utils.viewport.Viewport
import gingercat.owlbricks.logic.GameWorld
import gingercat.owlbricks.states.StateMachine

class MainGame : InputListener(), ApplicationListener {
    companion object {
        const val BRICK_SIZE = 31f
        const val LINES = 10
        const val COLUMNS = 7
        const val FRAME_WIDTH = BRICK_SIZE * COLUMNS
        const val FRAME_HEIGHT = BRICK_SIZE * LINES
        const val FRAME_X = - (FRAME_WIDTH / 2f)
        const val FRAME_Y = - (FRAME_HEIGHT / 2f)
        const val WORLD_WIDTH = 240f
        const val WORLD_HEIGHT = 360f
        const val PROGRESS_BAR_X = MainGame.FRAME_X
        const val PROGRESS_BAR_Y = MainGame.FRAME_Y - 10f
        const val PROGRESS_BAR_WIDTH = MainGame.FRAME_WIDTH
        const val PROGRESS_BAR_HEIGHT = 4f
    }

    private lateinit var camera: OrthographicCamera
    private lateinit var viewport: Viewport

    private lateinit var world: GameWorld
    private lateinit var stateMachine: StateMachine

    override fun create() {
        camera = OrthographicCamera()
        camera.position.set(0f, 0f, 0f)

        viewport = ExtendViewport(WORLD_WIDTH, WORLD_HEIGHT, camera)
        world = GameWorld(viewport)

        stateMachine = StateMachine(world)

        world.viewport.apply()

        Gdx.input.inputProcessor = world

        world.addListener(this)
    }

    override fun resize(width: Int, height: Int) {
        world.viewport.update(width, height)

        world.camera.position.set(0f, 0f, 0f)
    }

    override fun render() {
        world.batch.projectionMatrix = world.camera.combined

        world.camera.update()

        val delta = Gdx.graphics.deltaTime

        stateMachine.update(delta)

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        world.act(delta)
        world.draw()
    }

    override fun dispose() {
    }

    override fun pause() {
    }

    override fun resume() {
    }

    override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int): Boolean {
        stateMachine.touchDown(x, y)
        return true
    }

    override fun touchUp(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int) {
        stateMachine.touchUp(x, y)
    }

    override fun touchDragged(event: InputEvent?, x: Float, y: Float, pointer: Int) {
        stateMachine.touchDragged(x, y)
    }
}
