package gingercat.owlbricks.logic

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.Actor
import gingercat.owlbricks.MainGame

class GameFrame: Actor() {
    private val renderer = ShapeRenderer()

    init {
        width = MainGame.FRAME_WIDTH
        height = MainGame.FRAME_HEIGHT
        x = MainGame.FRAME_X
        y = MainGame.FRAME_Y
    }

    override fun draw(batch: Batch?, parentAlpha: Float) {
        if (batch == null)
            return

        batch.end()

        renderer.projectionMatrix = batch.projectionMatrix

        renderer.begin(ShapeRenderer.ShapeType.Filled)
        renderer.color = Color.GRAY
        renderer.rect(x, y, width, height)
        renderer.end()

        renderer.begin(ShapeRenderer.ShapeType.Line)
        renderer.color = Color.WHITE
        renderer.rect(x, y, width, height)
        renderer.end()

        batch.begin()
    }
}