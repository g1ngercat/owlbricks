package gingercat.owlbricks.logic

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.Actor
import gingercat.owlbricks.MainGame

class ProgressBar: Actor() {
    private val renderer = ShapeRenderer()

    private val levelScores = arrayOf(0, 50, 120, 250, 400, Int.MAX_VALUE)

    private var maxScore = levelScores[1]
    var level = 1

    var score: Int = 0

    init {
        x = MainGame.PROGRESS_BAR_X
        y = MainGame.PROGRESS_BAR_Y
        width = MainGame.PROGRESS_BAR_WIDTH
        height = MainGame.PROGRESS_BAR_HEIGHT
    }

    override fun act(delta: Float) {
        maxScore = levelScores.first { s -> s > score }
        level = levelScores.indexOf(maxScore)
    }

    override fun draw(batch: Batch?, parentAlpha: Float) {
        if (batch == null)
            return

        batch.end()

        renderer.projectionMatrix = batch.projectionMatrix

        val w = width * (score % maxScore) / maxScore

        renderer.begin(ShapeRenderer.ShapeType.Filled)
        renderer.color = Color.RED
        renderer.rect(x, y, w, height)
        renderer.end()

        renderer.begin(ShapeRenderer.ShapeType.Line)
        renderer.color = Color.WHITE
        renderer.rect(x, y, width, height)
        renderer.end()

        batch.begin()
    }
}