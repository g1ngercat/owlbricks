package gingercat.owlbricks.logic

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.Actor

class Owl(val score: Int) : Actor() {
    private val renderer = ShapeRenderer()

    override fun draw(batch: Batch?, parentAlpha: Float) {
        if (batch == null)
            return

        batch.end()

        renderer.projectionMatrix = batch.projectionMatrix

        renderer.begin(ShapeRenderer.ShapeType.Filled)
        renderer.color = color
        renderer.rect(x, y, width, height)
        renderer.end()

        renderer.begin(ShapeRenderer.ShapeType.Line)
        renderer.color = Color.WHITE
        renderer.rect(x, y, width, height)
        renderer.end()

        batch.begin()
    }
}