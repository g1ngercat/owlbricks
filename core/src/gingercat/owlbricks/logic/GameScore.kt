package gingercat.owlbricks.logic

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.scenes.scene2d.Actor
import gingercat.owlbricks.MainGame

class GameScore: Actor() {
    private val font = BitmapFont()

    var score = 0
    var level = 1

    override fun draw(batch: Batch?, parentAlpha: Float) {
        val text = "Score: $score, Level: $level"
        font.draw(batch, text, MainGame.FRAME_X, MainGame.FRAME_Y + MainGame.FRAME_HEIGHT + font.lineHeight)
    }
}