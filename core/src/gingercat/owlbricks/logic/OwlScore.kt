package gingercat.owlbricks.logic

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.scenes.scene2d.Actor

class OwlScore(private val score: Int, val destroyAt: Long): Actor() {
    private val font = BitmapFont()

    override fun draw(batch: Batch?, parentAlpha: Float) {
        font.color = Color(1f, 1f, 1f, color.a)
        font.draw(batch, "+$score", x, y)
    }
}