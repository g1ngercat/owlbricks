package gingercat.owlbricks.logic

import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.Viewport
import gingercat.owlbricks.MainGame

class GameWorld(viewport: Viewport): Stage(viewport) {
    private val frame = GameFrame()
    private val gameScore = GameScore()
    private val progressBar = ProgressBar()

    var pendingNewRow = false
    var gameOver = false

    init {
        this.addActor(frame)
        this.addActor(gameScore)
        this.addActor(progressBar)
    }

    fun isFilled(): Boolean {
        var maxY = MainGame.FRAME_Y
        for (actor in actors) {
            if (actor is Owl) {
                val top = actor.y + actor.height
                if (top > maxY)
                    maxY = top
            }
        }
        return maxY >= (MainGame.FRAME_Y + MainGame.FRAME_HEIGHT - 0.001f)
    }

    fun reset() {
        gameScore.score = 0
        progressBar.score = 0
        gameOver = false
        actors.filter { a -> a is Owl }
                .forEach { a -> a.remove() }
    }

    fun addScore(score: Int) {
        progressBar.score += score
        gameScore.score = progressBar.score
        gameScore.level = progressBar.level

    }
}