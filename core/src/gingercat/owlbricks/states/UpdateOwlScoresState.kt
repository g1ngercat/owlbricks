package gingercat.owlbricks.states

import com.badlogic.gdx.utils.TimeUtils
import gingercat.owlbricks.logic.GameWorld
import gingercat.owlbricks.logic.OwlScore

class UpdateOwlScoresState(private val world: GameWorld) : GameStateBase() {
    override fun update(delta: Float): Boolean {
        val now = TimeUtils.millis()

        for (actor in world.actors.filter { a -> a is OwlScore && a.destroyAt <= now }) {
            actor.remove()
        }

        return false
    }
}