package gingercat.owlbricks.states

abstract class GameStateBase {
    open fun update(delta: Float): Boolean {
        return true
    }

    open fun enter() {
    }

    open fun exit() {
    }

    open fun touchUp(x: Float, y: Float) {
    }

    open fun touchDown(x: Float, y: Float) {
    }

    open fun touchDragged(x: Float, y: Float) {
    }
}