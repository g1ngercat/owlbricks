package gingercat.owlbricks.states

import com.badlogic.gdx.scenes.scene2d.Actor
import gingercat.owlbricks.MainGame
import gingercat.owlbricks.logic.GameWorld
import gingercat.owlbricks.logic.Owl

class DragState(private val world: GameWorld)
    : GameStateBase() {

    private var finished = false

    private var offsetX = 0f
    private var initialX = 0f
    private var marginLeft = 0f
    private var marginRight = 0f

    private var actor: Actor? = null

    override fun touchUp(x: Float, y: Float) {
        if (actor == null)
            return

        val posX = Math.round((actor!!.x - MainGame.FRAME_X) / MainGame.BRICK_SIZE)
        actor!!.x = posX * MainGame.BRICK_SIZE + MainGame.FRAME_X
        finished = true
    }

    override fun enter() {
        finished = false
    }

    override fun update(delta: Float): Boolean {
        return finished
    }

    override fun exit() {
        world.pendingNewRow = true
    }

    override fun touchDragged(x: Float, y: Float) {
        if (actor == null)
            return

        actor!!.x = x - offsetX
        actor!!.x = Math.max(actor!!.x, marginLeft)
        actor!!.x = Math.min(marginRight - actor!!.width, actor!!.x)
    }

    private fun getLeftDragMargin(): Float {
        val rowObjects = getRowActors(actor!!.y + (MainGame.BRICK_SIZE / 2f))
        var left = MainGame.FRAME_X
        for (rowObject in rowObjects) {
            if (rowObject == actor)
                continue
            if (rowObject.x + rowObject.width <= actor!!.x)
                left = Math.max(left, rowObject.x + rowObject.width)
        }
        return left
    }

    private fun getRightDragMargin(): Float {
        val rowActors = getRowActors(actor!!.y + (MainGame.BRICK_SIZE / 2f))
        var right = MainGame.FRAME_X + MainGame.FRAME_WIDTH
        for (rowActor in rowActors) {
            if (rowActor == actor)
                continue
            if (rowActor.x >= actor!!.x + actor!!.width)
                right = Math.min(right, rowActor.x)
        }
        return right
    }

    private fun getRowActors(y: Float): List<Actor> {
        val rowObjects = mutableListOf<Actor>()

        for (actor in world.actors) {
            if (actor is Owl) {
                if (y >= actor.y && y <= (actor.y + actor.height))
                    rowObjects.add(actor)
            }
        }

        return rowObjects
    }

    override fun touchDown(x: Float, y: Float) {
        actor = getActorAt(x, y)
        if (actor != null) {
            offsetX = x - actor!!.x
            initialX = actor!!.x
            marginLeft = getLeftDragMargin()
            marginRight = getRightDragMargin()
        }
        return
    }

    private fun getActorAt(x: Float, y: Float): Actor? {
        for (actor in world.actors) {
            if (actor is Owl) {
                if (x >= actor.x && x <= actor.x + actor.width
                        && y >= actor.y && y <= actor.y + actor.height)
                    return actor
            }
        }
        return null
    }
}