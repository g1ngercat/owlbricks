package gingercat.owlbricks.states

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.utils.TimeUtils
import gingercat.owlbricks.MainGame
import gingercat.owlbricks.logic.GameWorld
import gingercat.owlbricks.logic.Owl

class AddRowState(private val world: GameWorld): GameStateBase() {

    companion object {
        const val MOVE_UP_DURATION = .14f
    }

    private var endTime: Long = 0

    override fun enter() {
        world.pendingNewRow = false
        for (actor in world.actors.filter { a -> a is Owl }) {
            actor.addAction(Actions.moveBy(0f, MainGame.BRICK_SIZE, MOVE_UP_DURATION))
        }
        endTime = TimeUtils.millis() + (MOVE_UP_DURATION * 1000).toLong()
    }

    override fun update(delta: Float): Boolean {
        val timeMs = TimeUtils.millis()

        if (timeMs >= endTime) {
            addRandomBlocks()
            return true
        }

        return false
    }

    private fun addRandomBlocks() {
        var xSpace = (0..2).random()
        var size = (1..4).random()
        var color = getBrickColor(size)
        while (xSpace + size <= (MainGame.FRAME_WIDTH/ MainGame.BRICK_SIZE)) {
            val owl = Owl(size)
            owl.color = color
            owl.x = xSpace * MainGame.BRICK_SIZE + MainGame.FRAME_X
            owl.y = MainGame.FRAME_Y
            owl.width = size * MainGame.BRICK_SIZE
            owl.height = MainGame.BRICK_SIZE
            world.addActor(owl)
            xSpace += size + (1..5).random()
            size = (1..4).random()
            color = getBrickColor(size)
        }
    }

    private fun getBrickColor(size: Int): Color {
        return when (size) {
            1 -> Color.ORANGE
            2 -> Color.NAVY
            3 -> Color.BROWN
            4 -> Color.FOREST
            else -> Color.WHITE
        }
    }
}