package gingercat.owlbricks.states

import gingercat.owlbricks.logic.GameWorld
import gingercat.owlbricks.logic.Owl

class StateManager(private val world: GameWorld) {
    private val addRowState = AddRowState(world)
    private val fallDownState = FallDownState(world)
    private val dragState = DragState(world)
    private val removeLinesState = RemoveLinesState(world)
    private val gameOverState = GameOverState(world)
    private val showOwlScoresState = UpdateOwlScoresState(world)

    private val transitions = listOf(
            StateTransition(addRowState, ::addRowStateFinished),
            StateTransition(fallDownState, removeLinesState),
            StateTransition(removeLinesState, ::removeLinesStateFinished),
            StateTransition(dragState, fallDownState),
            StateTransition(gameOverState, addRowState)
    )

    private fun addRowStateFinished(): GameStateBase {
        if (world.isFilled())
            return gameOverState
        if (fallDownState.getFallActors().any())
            return fallDownState
        return dragState
    }

    private fun removeLinesStateFinished(): GameStateBase {
        if (fallDownState.getFallActors().any())
            return fallDownState
        if (world.pendingNewRow || world.actors.none { a -> a is Owl })
            return addRowState
        return dragState
    }

    val initialStates: List<GameStateBase>
        get() = listOf(addRowState, showOwlScoresState)

    fun nextState(from: GameStateBase): GameStateBase {
        for (transition in transitions) {
            if (transition.from == from)
                return transition.to
        }
        throw Exception("Missing state")
    }
}