package gingercat.owlbricks.states

import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.utils.TimeUtils
import gingercat.owlbricks.MainGame
import gingercat.owlbricks.logic.GameWorld
import gingercat.owlbricks.logic.Owl
import gingercat.owlbricks.logic.OwlScore

class RemoveLinesState(private val world: GameWorld): GameStateBase() {

    companion object {
        const val SHOW_SCORES_DURATION = 1f
    }

    private var fullLines: List<List<Actor>>? = null

    override fun enter() {
        val lines = getFullLines()
        if (lines.any()) {
            for (line in lines) {
                line.forEach { a -> if (a is Owl) world.addScore(a.score) }
            }
            fullLines = lines
        }
    }

    override fun exit() {
        if (fullLines != null) {
            for (line in fullLines!!) {
                val now = TimeUtils.millis()
                for (actor in line) {
                    if (actor is Owl) {
                        val owlScore = OwlScore(actor.score, now + (SHOW_SCORES_DURATION * 1000).toLong())
                        owlScore.x = actor.x
                        owlScore.y = actor.y + actor.height
                        owlScore.addAction(Actions.alpha(0f, SHOW_SCORES_DURATION))
                        owlScore.addAction(Actions.moveBy(0f, actor.height, SHOW_SCORES_DURATION))
                        world.addActor(owlScore)
                        actor.remove()
                    }
                }
            }
            fullLines = null
        }
    }

    private fun getFullLines(): List<List<Actor>> {
        val lines = mutableListOf<List<Actor>>()

        world.actors
                .filter { a -> a is Owl }
                .groupBy { a -> MathUtils.round(a.y) }
                .filter { g -> g.value.sumBy { a -> MathUtils.floor(a.width) } >= MainGame.COLUMNS * MainGame.BRICK_SIZE }
                .forEach { g -> lines.add(g.value) }

        return lines
    }
}