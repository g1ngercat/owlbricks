package gingercat.owlbricks.states

import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.utils.TimeUtils
import gingercat.owlbricks.MainGame
import gingercat.owlbricks.logic.GameWorld
import gingercat.owlbricks.logic.Owl

class FallDownState(private val world: GameWorld)
    : GameStateBase() {
    companion object {
        const val FALL_DOWN_DURATION = .14f
    }

    private var endTime: Long = 0

    override fun update(delta: Float): Boolean {
        val timeMs = TimeUtils.millis()
        if (timeMs > endTime) {
            val fallActors = getFallActors()
            if (fallActors.isEmpty())
                return true
            else {
                for (fallActor in fallActors)
                    fallActor.addAction(Actions.moveBy(0f, -MainGame.BRICK_SIZE, FALL_DOWN_DURATION))
                endTime = timeMs + (FALL_DOWN_DURATION * 1000).toLong()
            }
        }

        return false
    }

    fun getFallActors(): List<Actor> {
        val fallActors = mutableListOf<Actor>()

        val staticActors = mutableListOf<Actor>()
        staticActors.addAll(world.actors.filter { a -> a is Owl })

        var fallActor = findFallActor(staticActors)

        while (fallActor != null) {
            fallActors.add(fallActor)
            staticActors.remove(fallActor)
            fallActor = findFallActor(staticActors)
        }

        return fallActors
    }

    private fun findFallActor(actors: List<Actor>): Actor? {
        val baseRectangle = Rectangle(
                MainGame.FRAME_X,
                MainGame.FRAME_Y - MainGame.BRICK_SIZE,
                MainGame.FRAME_WIDTH,
                MainGame.BRICK_SIZE)

        for (actor in actors) {
            val rectangle = getRectangle(actor)

            rectangle.y -= (MainGame.BRICK_SIZE / 2f)

            if (rectangle.overlaps(baseRectangle))
                continue

            var overlaps = false

            for (otherActor in actors) {
                if (otherActor != actor) {
                    if (getRectangle(otherActor).overlaps(rectangle)) {
                        overlaps = true
                        break
                    }
                }
            }

            if (!overlaps)
                return actor
        }

        return null
    }

    private fun getRectangle(actor: Actor): Rectangle {
        return Rectangle(actor.x, actor.y, actor.width, actor.height)
    }
}