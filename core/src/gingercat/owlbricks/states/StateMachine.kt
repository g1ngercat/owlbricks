package gingercat.owlbricks.states

import gingercat.owlbricks.logic.GameWorld

class StateMachine(world: GameWorld) {
    private val stateManager = StateManager(world)

    private val states = stateManager.initialStates.toMutableList()

    init {
        states.forEach { s -> s.enter() }
    }

    fun update(delta: Float) {
        val finishedStates = mutableListOf<GameStateBase>()

        states.forEach { s -> if (s.update(delta)) finishedStates.add(s) }

        for (state in finishedStates) {
            state.exit()
            states.remove(state)

            val newState = stateManager.nextState(state)
            newState.enter()
            states.add(newState)
        }
    }

    fun touchUp(x: Float, y: Float) {
        states.forEach { s -> s.touchUp(x, y) }
    }

    fun touchDown(x: Float, y: Float) {
        states.forEach { s -> s.touchDown(x, y) }
    }

    fun touchDragged(x: Float, y: Float) {
        states.forEach { s -> s.touchDragged(x, y) }
    }
}