package gingercat.owlbricks.states

class StateTransition {
    val from: GameStateBase
    private val condition: (() -> GameStateBase)

    constructor(from: GameStateBase, condition: () -> GameStateBase) {
        this.from = from
        this.condition = condition
    }

    constructor(from: GameStateBase, to: GameStateBase) {
        this.from = from
        this.condition = { to }
    }

    val to: GameStateBase
        get() = condition.invoke()
}