package gingercat.owlbricks.states

import gingercat.owlbricks.logic.GameWorld

class GameOverState(private val world: GameWorld): GameStateBase() {
    private var finished = false

    override fun enter() {
        finished = false
        world.gameOver = true
    }

    override fun update(delta: Float): Boolean {
        return finished
    }

    override fun touchDown(x: Float, y: Float) {
        world.reset()
        finished = true
    }
}
